package com.sergeyefimov.study.player.view;

import com.sergeyefimov.study.player.model.Player;
import lombok.NonNull;
import org.springframework.stereotype.Component;

@Component
public final class PlayerViewImpl implements PlayerView {
    @Override
    public void printPlayer(@NonNull Player player) {
        System.out.println("- Player: " + player.getName() + ". Account amount: " + player.getAccount());
    }
}
