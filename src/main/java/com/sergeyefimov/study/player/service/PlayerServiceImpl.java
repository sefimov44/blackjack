package com.sergeyefimov.study.player.service;

import com.sergeyefimov.study.player.model.Player;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public final class PlayerServiceImpl implements PlayerService {
    @Override
    public @NonNull Player createPlayer(@NonNull BigDecimal accountAmount, @NonNull String name) {
        return new Player(accountAmount, name);
    }

    @Override
    public void increaseAccountAmount(@NonNull Player player, @NonNull BigDecimal amount) {
        player.setAccount(player.getAccount().add(amount));
    }

    @Override
    public void decreaseAccountAmount(@NonNull Player player, @NonNull BigDecimal amount) {
        increaseAccountAmount(player, amount.negate());
    }

    @Override
    public boolean doesPlayerHaveMoney(@NonNull Player player, @NonNull BigDecimal amount) {
        return player.getAccount().compareTo(amount) >= 0;
    }
}
