package com.sergeyefimov.study.dealer.service;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Hand;
import lombok.NonNull;

public interface DealerService {
    int INIT_CARD_NUMBER = 2;

    @NonNull
    Dealer createDealer();

    void initialize(@NonNull Dealer dealer);

    void cardDistribution(@NonNull Dealer dealer, @NonNull Hand hand);

    void addCardToHand(@NonNull Dealer dealer, @NonNull Hand hand);

    void dealToYourself(@NonNull Dealer dealer, @NonNull Hand dealerHand);
}
