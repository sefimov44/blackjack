package com.sergeyefimov.study.dealer.service;

import com.sergeyefimov.study.card.DeckOfCardsFactory;
import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Hand;
import com.sergeyefimov.study.hand.service.HandService;
import com.sergeyefimov.study.hand.model.State;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.sergeyefimov.study.hand.service.HandService.MAX_SCORE;

@Service
public final class DealerServiceImpl implements DealerService {
    private static final int STOP_THRESHOLD = 16;

    private final DeckOfCardsFactory deckOfCardsFactory;
    private final HandService handService;

    @Autowired
    public DealerServiceImpl(DeckOfCardsFactory deckOfCardsFactory, HandService handService) {
        this.deckOfCardsFactory = deckOfCardsFactory;
        this.handService = handService;
    }

    @Override
    @NonNull
    public Dealer createDealer() {
        return new Dealer();
    }

    @Override
    public void initialize(@NonNull Dealer dealer) {
        dealer.setDeckOfCards(deckOfCardsFactory.getDeskOfCards());
    }

    @Override
    public void cardDistribution(Dealer dealer, Hand hand) {
        for (int i = 0; i < INIT_CARD_NUMBER; i++) {
            handService.addCard(hand, dealer.getCard());
        }
    }

    @Override
    public void addCardToHand(@NonNull Dealer dealer, @NonNull Hand hand) {
        handService.addCard(hand, dealer.getCard());
    }

    @Override
    public void dealToYourself(@NonNull Dealer dealer, @NonNull Hand dealerHand) {
        while ((handService.getHardScore(dealerHand) <= STOP_THRESHOLD ) ||
                (handService.getHardScore(dealerHand) > MAX_SCORE && handService.getSoftScore(dealerHand) <= STOP_THRESHOLD )) {
            handService.addCard(dealerHand, dealer.getCard());
        }
        if (handService.getHardScore(dealerHand) > MAX_SCORE && handService.getSoftScore(dealerHand) > MAX_SCORE) {
            handService.setState(dealerHand, State.BUSTED);
        } else {
            handService.setState(dealerHand, State.DONE);
        }
    }
}
