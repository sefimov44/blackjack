package com.sergeyefimov.study.round.controller;

import com.sergeyefimov.study.dealer.model.Dealer;
import com.sergeyefimov.study.hand.model.Command;
import com.sergeyefimov.study.player.model.Player;
import com.sergeyefimov.study.round.model.RoundResult;
import com.sergeyefimov.study.round.model.Round;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.Set;

public interface RoundController {
    @NonNull Round createRound(@NonNull Player player, @NonNull Dealer dealer, @NonNull BigDecimal bet);

    @NonNull Set<Command> getAvailableCommandsList(@NonNull Round round, int handIndex);

    void executeCommand(@NonNull Round round, int handIndex, @NonNull Command command);

    boolean isAllHandFinished(@NonNull Round round);

    void finishRound(@NonNull Round round);

    @NonNull RoundResult getHandResult(@NonNull Round round, int handIndex);
}
