package com.sergeyefimov.study.round.view;

import com.sergeyefimov.study.round.model.RoundResult;
import lombok.NonNull;
import org.springframework.stereotype.Component;

@Component
public final class RoundResultViewImpl implements RoundResultView {
    @Override
    public void printRoundResult(@NonNull RoundResult roundResult) {
        System.out.println(roundResult.getDescription());
    }
}
