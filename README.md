# blackjack
Simple BlackJack CLI implementation.  
- _Blackjack.java_ - main class (some sort of frontend emulation)  
- _src/*_ - backend  
Implemented using Spring Boot, not necessary for this game, but allows to migrate it to web implementation quickly.
To do this just rewrite controllers with Spring Web and enjoy.  
  
**Here are the main game roles:**
- _DeckOfCards_ - cards deck (in this implementation standard deck of 52 cards)  
- _Dealer_ - dealer, posses deck of cards and distribute them between hands  
- _Hand_ - can belong to player or dealer, contains cards within a round  
- _Player_ - contain information about a player (account amount and name)  
- _Round_ - contain information about current round (cards of all participants), execute commands from a user  
When the game is starting frontend sends a request to create a player and a dealer to player and dealer controllers respectively.
Then, frontend creates a request to create a new round, and then get available commands and execute them for each hand, until the round is not over.
After that frontend sends a command to finish the round and receives results for each hand.  
Rounds continue until user interrupt the game or lose all his money.

